package pw.itr0.todomvc.domain.todo;

import java.util.Collection;
import java.util.function.Predicate;

public interface TodoItemRepository {
    Collection<TodoItem> findAll();

    TodoItem insert(TodoItem todoItem);

    void delete(Long todoId);

    void update(TodoItem todoItem);

    TodoItem findOne(Long todoId);

    void remove(Predicate<TodoItem> predicate);
}
