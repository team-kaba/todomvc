package pw.itr0.todomvc.util;

import java.util.Properties;

public class PlatformHelper {
    public static final PlatformHelper instance = new PlatformHelper();

    private final Properties props;

    public PlatformHelper() {
        this(System.getProperties());
    }

    public PlatformHelper(final Properties props) {
        this.props = props;
    }

    private String property(final String name) {
        String value = this.props.getProperty(name);
        return value != null ? value : System.getProperty(name);
    }

    public String os() {
        final String name = property("os.name").toLowerCase();
        if (name.contains("windows")) {
            return "windows";
        }

        if (name.contains("mac")) {
            return "darwin";
        }

        if (name.contains("linux")) {
            return "linux";
        }

        if (name.contains("freebsd")) {
            return "linux";
        }

        if (name.contains("sunos")) {
            return "sunos";
        }

        throw new IllegalArgumentException("Unsupported OS: " + name);
    }

    public String arch() {
        final String arch = property("os.arch").toLowerCase();
        if (arch.contains("64")) {
            return "x64";
        } else {
            return "x86";
        }
    }

    public boolean isWindows() {
        return os().equals("windows");
    }
}
