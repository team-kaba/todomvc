package pw.itr0.todomvc.dev.node;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import pw.itr0.todomvc.util.PlatformHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@Profile("node-server")
@Component
@lombok.Value
@Slf4j
public class StupidNodeServerConfiguration {
    private static final String PREFIX = "node.";

    private final Path executable;

    private final Path serverRoot;

    private final Path server;

    private final Path cwd;

    @Autowired
    public StupidNodeServerConfiguration(
            @Value("${" + PREFIX + "cwd:}") String cwd,
            @Value("${" + PREFIX + "executable:}") String executable,
            @Value("${" + PREFIX + "server-root:.}") String serverRoot,
            @Value("${" + PREFIX + "server:}") String server,
            @Value("${" + PREFIX + "version:}") String version) throws FileNotFoundException {
        if (!StringUtils.hasText(cwd)) {
            cwd = System.getProperty("user.dir");
        }
        Path cwdPath = Paths.get(cwd);
        if (!Files.exists(cwdPath)) {
            throw new FileNotFoundException("node server working directory is not found. resolved path =["
                    + cwdPath + "]. " + this.toString());
        }
        this.cwd = cwdPath.toAbsolutePath();

        Path serverRootPath = cwdPath.resolve(serverRoot);
        if (!Files.exists(serverRootPath)) {
            throw new FileNotFoundException("node server root path is not found. resolved path =["
                    + serverRootPath + "]. " + this.toString());
        }
        this.serverRoot = serverRootPath.toAbsolutePath();

        Path executablePath = cwdPath.resolve(executable);
        if (!StringUtils.hasText(executable)) {
            executablePath = findNodeExecutable(serverRootPath, version);
        }
        if (!Files.exists(executablePath)) {
            throw new FileNotFoundException("node executable file is not found. resolved path =["
                    + executablePath + "]. " + this.toString());
        }
        this.executable = executablePath.toAbsolutePath();

        Path serverPath = cwdPath.resolve(server);
        if (!Files.exists(serverPath)) {
            throw new FileNotFoundException("node server.js file is not found. resolved path =["
                    + serverPath + "]. " + this.toString());
        }
        this.server = serverPath.toAbsolutePath();
    }

    private Path findNodeExecutable(Path serverRoot, String version) {
        PlatformHelper platform = PlatformHelper.instance;
        String os = platform.os();
        String arch = platform.arch();
        if (!StringUtils.hasText(version)) {
            try (Stream<String> lines = Files.lines(serverRoot.resolve(".node-version"))) {
                version = lines.findFirst().orElse("");
            } catch (IOException e) {
                version = "";
            }
        }

        String node = "node-" + version + "-" + os + "-" + arch;
        String ext = platform.isWindows() ? ".exe" : "";

        return Stream.of(home().resolve(".gradle/nodejs"), cwd.resolve("./nodejs"), cwd.resolve("../nodejs"))
                .map(d -> d.resolve(node).resolve("bin/node" + ext))
                .filter(Files::exists)
                .findFirst()
                .orElseGet(StupidNodeServerConfiguration::findNodeInPath);
    }

    private static Path findNodeInPath() {
        String[] path = System.getenv("PATH").split(Pattern.quote(File.pathSeparator));
        List<String> search = new ArrayList<>();
        search.addAll(Arrays.asList(path));
        final Path home = home();
        search.add(home.resolve(".anyenv/envs/ndenv/shims/").toAbsolutePath().toString());
        search.add(home.resolve(".ndenv/shims/").toAbsolutePath().toString());
        return search.stream().map(Paths::get)
                .map(p -> p.resolve("node"))
                .filter(Files::exists)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("No Node.js executables are found."));
    }

    private static Path home() {
        String home = System.getenv("HOME");
        if (home == null) {
            home = System.getenv("USERPROFILE");
        }
        return Paths.get(home);
    }
}
