package pw.itr0.todomvc.dev.node;

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.TimeUnit;

/**
 * Launch Node.js server on Spring Boot server start.
 */
@Profile("node-server")
@Component
@ToString(exclude = "proc")
@Slf4j
public class StupidNodeServerRunner {
    private static final String NODE_SERVER_STATUS = "node-server-status";

    private final String cwd;

    private final String executable;

    private final File serverRoot;

    private final String server;

    private Process proc;

    @Autowired
    public StupidNodeServerRunner(StupidNodeServerConfiguration config) {
        this.cwd = String.valueOf(config.getCwd());
        this.serverRoot = config.getServerRoot().toFile();
        this.executable = String.valueOf(config.getExecutable());
        this.server = String.valueOf(config.getServer());
    }

    @PostConstruct
    public synchronized void run() throws FileNotFoundException {
        String status = System.getProperty("node-server-status");
        if (status == null || "stopped".equals(status)) {
            log.info("Start Node.js server. Running in {}", cwd);
            log.info("Node.js executable is {}.", executable);
            log.info("Node.js server script is {}.", server);

            ProcessBuilder builder = new ProcessBuilder();
            log.info("Node.js server process environment variables are {}", builder.environment());
            try {
                proc = builder.command(executable, server)
                        .directory(serverRoot)
                        .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                        .redirectError(ProcessBuilder.Redirect.INHERIT)
                        .start();
                log.info("Started Node.js server.");
                System.setProperty(NODE_SERVER_STATUS, "running");
            } catch (Exception e) {
                throw new IllegalStateException("Failed to start Node Server command. " + this.toString(), e);
            }
        }
    }

    @PreDestroy
    public synchronized void stop() {
        if (proc != null) {
            try {
                log.info("Stopping Node.js server.");
                boolean exit = proc.destroyForcibly().waitFor(10, TimeUnit.SECONDS);
                if (exit) {
                    log.info("Successfully stopped Node.js server.");
                    System.setProperty(NODE_SERVER_STATUS, "stopped");
                    return;
                }
                abort();
            } catch (InterruptedException e) {
                abort();
                throw new IllegalStateException("Interrupted while stopping Node Server command. " + this.toString(), e);
            }
        }
    }

    private void abort() {
        log.info("Failed to stop Node.js server. Force stopping server.");
        proc.destroy();
        System.setProperty(NODE_SERVER_STATUS, "aborted");
    }
}
