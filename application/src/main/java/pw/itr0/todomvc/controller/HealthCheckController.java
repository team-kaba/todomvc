package pw.itr0.todomvc.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * A controller to handle health check requests.
 *
 * @author ryotan
 */
@RestController
@RequestMapping("health-check")
public class HealthCheckController {

    /**
     * Handles health check requests. Only responses 200 OK.
     *
     * @return 200 OK
     */
    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<Void> api() {
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
