var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

const app = {
  root: path.resolve(__dirname, '../src/main/web')
};

module.exports = {
  devtool: ['source-map'],
  entry: [path.resolve(app.root, 'app.tsx')],
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'bundle.dev.[hash].js'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: 'jade!src/main/web/index.jade'
    }),
    new ExtractTextPlugin("bundle.dev.[hash].css", {allChunks: true})
  ],
  resolve: {
    extensions: ['', '.js', '.ts', '.tsx']
  },
  module: {
    loaders: [{
      test: /\.tsx?$/,
      loaders: ['ts'],
      include: app.root
    }, {
      test: /\.jade$/,
      loaders: ['jade-react'],
      include: app.root,
      exclude: path.resolve(app.root, 'index.jade')
    }, {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract('sass', 'css'),
      include: app.root
    }]
  },
  stats: {
    colors: true,
    reasons: true
  },
  debug: false
};

