var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

const app = {
  root: path.resolve(__dirname, '../src/main/web')
};

module.exports = {
  entry: [path.resolve(app.root, 'app.tsx')],
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'bundle.[hash].js'
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
    new HtmlWebpackPlugin({
      template: 'jade!src/main/web/index.jade'
    }),
    new ExtractTextPlugin("bundle.[hash].css", {allChunks: true})
  ],
  resolve: {
    extensions: ['', '.js', '.ts', '.tsx']
  },
  module: {
    loaders: [{
      test: /\.tsx?$/,
      loaders: ['ts'],
      include: app.root
    }, {
      test: /\.jade$/,
      loaders: ['jade-react'],
      include: app.root,
      exclude: path.resolve(app.root, 'index.jade')
    }, {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract('sass', 'css'),
      include: app.root
    }]
  },
  stats: {
    colors: true,
    reasons: true
  },
  debug: false
};
