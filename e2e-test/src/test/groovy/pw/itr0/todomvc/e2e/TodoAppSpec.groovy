package pw.itr0.todomvc.e2e

import geb.spock.GebReportingSpec
import spock.lang.Stepwise
import spock.lang.Title

@Title("Todo MVC アプリケーション全体のシナリオテスト")
@Stepwise
class TodoAppSpec extends GebReportingSpec {

  def "トップページを開く。"() {
    setup: 'TODOが保存されていない状態で'

    when: 'トップページを開くと'
    to TodoAppTopPage

    then: 'トップページが表示されること。'
    at TodoAppTopPage

    and: 'TODOが空であること。'
    main.todos.size() == 0

    and: 'フッターの残りのTODO数が0であること。'
    footer.itemsLeftCount() == '0'

    and: 'フッターに一括削除ボタンが表示されていないこと。'
    !footer.hasAllDeleteButton()

    and: '全てのTODOの完了状態をトグルするボタンが表示されていないこと。'
    !main.hasToggleAll()
  }

  def "TODOを追加する。"() {
    when: '入力欄に入力してEnterキーを押したときに、'
    createTodo 'todo item1'

    then: '画面遷移はしないこと。'
    at TodoAppTopPage

    and: '入力欄は空になっていること。'
    header.input == ''

    and: '新しいTODOがリストに追加されていること。'
    main.todos.size() == 1
    def (todo) = main.todos
    todo.name() == 'todo item1'
    !todo.isCompleted()

    and: 'フッターに残りのTODO数が表示されていること。'
    footer.itemsLeftCount() == "${main.todos.size()}"
    !footer.hasAllDeleteButton()

    and: '全てのTODOの完了状態をトグルするボタンが表示されていること。'
    main.hasToggleAll()
  }

  def "TODOをさらに追加する。"() {
    when: '入力欄に入力してEnterキーを押したときに、'
    createTodo 'todo item2'
    createTodo 'todo item3'
    createTodo 'todo item4'
    createTodo 'todo item5'

    then: '新しいTODOがリストに追加されていること。'
    main.todos.size() == 5
    def (todo1, todo2, todo3, todo4, todo5) = main.todos
    todo1.name() == 'todo item1'
    !todo1.isCompleted()
    todo2.name() == 'todo item2'
    !todo2.isCompleted()
    todo3.name() == 'todo item3'
    !todo3.isCompleted()
    todo4.name() == 'todo item4'
    !todo4.isCompleted()
    todo5.name() == 'todo item5'
    !todo5.isCompleted()

    and: 'フッターに残りのTODO数が表示されていること。'
    footer.itemsLeftCount() == "${main.todos.size()}"
    !footer.hasAllDeleteButton()
  }

  def "TODOを一つ完了する。"() {
    when: 'TODOを一つ完了した時に、'
    completeTodo(1)

    then: 'TODOはリストには残っていること。'
    def (todo1, todo2, todo3, todo4, todo5) = main.todos
    main.todos.size() == 5

    and: 'TODOが完了状態で表示されていること。'
    !todo1.isCompleted()
    todo2.isCompleted()
    !todo3.isCompleted()
    !todo4.isCompleted()
    !todo5.isCompleted()

    and: 'フッターに残りのTODO数が表示されていること。'
    footer.itemsLeftCount() == '4'

    and: 'フッターに一括削除ボタンが表示されていること。'
    footer.hasAllDeleteButton()
  }

  def "完了したTODOを削除する。"() {
    when: '完了したTODOを一つ削除した時に、'
    deleteTodo(1)

    then: 'TODOはリストから消えること。'
    main.todos.size() == 4

    and: 'フッターに残りのTODO数が表示されていること。'
    footer.itemsLeftCount() == "${main.todos.size()}"

    and: '完了済みTODOがなくなるので、フッターから一括削除ボタンが消えること。'
    !footer.hasAllDeleteButton()
  }

  def "完了していないTODOを削除する。"() {
    when: '完了していないTODOを削除した時に、'
    deleteTodo(1)

    then: 'TODOはリストから消えること。'
    main.todos.size() == 3

    and: 'フッターに残りのTODO数が表示されていること。'
    footer.itemsLeftCount() == "${main.todos.size()}"

    and: '完了済みTODOがないので、フッターから一括削除ボタンが消えること。'
    !footer.hasAllDeleteButton()
  }

  def "完了していないTODOを編集する。"() {
    when: '完了したTODOを編集した時に、'
    editTodo(0, 'todo item1 edited')

    then: 'TODOのラベルが変化すること。'
    def (todo1) = main.todos
    todo1.name() == 'todo item1 edited'

    and: 'TODOの数は変化しないこと。'
    main.todos.size() == 3

    and: 'TODOの完了状態は変化しないこと。'
    footer.itemsLeftCount() == "${main.todos.size()}"
    !todo1.isCompleted()
    !footer.hasAllDeleteButton()
  }

  def "残りのTODOを全て完了する。"() {
    when: '残りの全てのTODOを完了すると'
    completeAllTodos()

    then: 'TODOの数は変化しないこと。'
    main.todos.size() == 3

    and: 'TODOが全て完了状態になっていること。'
    main.todos.eachWithIndex { todo, idx ->
      // Closureの中だと、戻り値がbooleanのstatementにならないので、Spockにassertionとして理解してもらえない。
      // 明示的にassertする必要がある。
      assert todo.isCompleted(), "todo $idx"
    }
    footer.itemsLeftCount() == '0'
    footer.hasAllDeleteButton()
  }

  def "完了したTODOを編集する。"() {
    when: '完了済みのTODOを編集すると'
    editTodo(2, 'todo item2 edited')

    then: 'TODOのラベルが変化すること。'
    main.todos[2].name() == 'todo item2 edited'

    and: 'TODOの数は変化しないこと。'
    main.todos.size() == 3

    and: 'TODOの完了状態は変化しないこと。'
    main.todos.eachWithIndex { todo, idx ->
      // Closureの中だと、戻り値がbooleanのstatementにならないので、Spockにassertionとして理解してもらえない。
      // 明示的にassertする必要がある。
      assert todo.isCompleted(), "todo $idx"
    }
    footer.itemsLeftCount() == '0'
    footer.hasAllDeleteButton()
  }

  def "完了したTODOを全て削除する。"() {
    when: '完了したTODOを全て削除すると'
    deleteAllCompleted()

    then: 'TODOが空になること。'
    main.todos.size() == 0
    footer.itemsLeftCount() == '0'
    !footer.hasAllDeleteButton()
    !main.hasToggleAll()
  }
}
