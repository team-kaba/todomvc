package pw.itr0.todomvc.e2e

import geb.Module

class Footer extends Module {
  public static final String DELETE_BUTTON_QUERY = 'button.clear-completed'

  static content = {
    items { $('span.todo-count strong') }
    button { $(DELETE_BUTTON_QUERY) }
  }

  def itemsLeftCount() {
    items.text()
  }

  def deleteAllCompleted() {
    button.click()
  }

  boolean hasAllDeleteButton() {
    !$(DELETE_BUTTON_QUERY).empty
  }
}
