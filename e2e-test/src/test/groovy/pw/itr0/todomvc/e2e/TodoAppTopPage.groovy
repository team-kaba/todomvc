package pw.itr0.todomvc.e2e

import geb.Page
import org.openqa.selenium.Keys

class TodoAppTopPage extends Page {
  static at = { title == "Todo MVC App" }

  static content = {
    header { module Header, $('header') }
    main { module MainSection, $('section.main') }
    footer { module Footer, $('footer') }
  }

  def createTodo(String todo) {
    header.input << todo << Keys.RETURN
  }

  def editTodo(int order, String edited) {
    def todo = main.todos[order]
    todo.edit(edited)
  }

  def completeTodo(int order) {
    def todo = main.todos[order]
    todo.complete()
  }

  def deleteTodo(int order) {
    def todo = main.todos[order]
    todo.delete()
  }

  def completeAllTodos() {
    main.completeAll()
  }

  def deleteAllCompleted() {
    footer.deleteAllCompleted()
  }
}
