package pw.itr0.todomvc.e2e

import geb.Module
import org.openqa.selenium.Keys

class TodoItem extends Module {
  static content = {
    toggle { $('input[type="checkbox"].toggle') }
    label { $('label') }
    editField { $('input.edit') }
    destroy { $('button.destroy') }
  }

  def name() {
    label.text()
  }

  def edit(String edited) {
    interact {
      moveToElement(label)
      doubleClick(label)
    }
    editField.value(edited) << Keys.RETURN
  }

  def complete() {
    toggle.click()
  }

  def delete() {
    interact {
      moveToElement(this)
    }
    destroy.click()
  }

  boolean isCompleted() {
    this.hasClass('completed')
  }
}
