package pw.itr0.todomvc.e2e

import geb.Module

class MainSection extends Module {
  private static final String TOGGLE_ALL_QUERY = 'input#toggle-all'

  static content = {
    toggle { $(TOGGLE_ALL_QUERY) }
    todos { moduleList TodoItem, $('ul.todo-list li') }
  }

  def completeAll() {
    toggle.click()
  }

  def hasToggleAll() {
    !$(TOGGLE_ALL_QUERY).empty
  }
}
