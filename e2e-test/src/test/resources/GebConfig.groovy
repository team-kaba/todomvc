/*
	This is the Geb configuration file.

	See: http://www.gebish.org/manual/current/#configuration
*/


import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.edge.EdgeDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.phantomjs.PhantomJSDriver
import org.openqa.selenium.safari.SafariDriver

waiting {
  timeout = 2
}

environments {

  // run via “./gradlew chromeTest”
  // See: http://code.google.com/p/selenium/wiki/ChromeDriver
  chrome {
    driver = { new ChromeDriver() }
  }

  // run via “./gradlew firefoxTest”
  // See: http://code.google.com/p/selenium/wiki/FirefoxDriver
  firefox {
    driver = { new FirefoxDriver() }
  }

  ie {
    driver = { new InternetExplorerDriver() }
  }

  edge {
    driver = { new EdgeDriver() }
  }

  safari {
    driver = { new SafariDriver() }
  }

  phantomjs {
    driver = { new PhantomJSDriver() }
  }

}

// To run the tests with all browsers just run “./gradlew test”

baseUrl = "http://localhost:3000"
