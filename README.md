# Kaba application template with [Spring Boot](http://projects.spring.io/spring-boot/) and [React](https://facebook.github.io/react/)

## What it does

* Hot swaps Java components ([spring-boot-devtools](http://docs.spring.io/spring-boot/docs/1.3.x/reference/html/using-boot-devtools.html))
* Hot reloads React components ([react-transform-hmr](https://github.com/gaearon/react-transform-hmr))
* [TypeScript](http://www.typescriptlang.org/)
* [Scss](http://sass-lang.com/) files to CSS
* [Jade](http://jade-lang.com/) template for React
* Build files with [webpack](https://webpack.github.io/)


## Getting things up




## CLI Commands



## Build and run product



## Development guidelines


## How to

### Update Gradle version

Update version of `distributionUrl` in `gradle/wrapper/gradle-wrapper.properties`.
